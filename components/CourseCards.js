// base imports
import { useState, useEffect } from 'react';
import PropTypes from 'prop-types';

// bootstrap
import { Card, Button } from 'react-bootstrap';

export default function CourseCard({ courseProp }){
	//console.log(course);
	const { name, description, price, start_date, end_date } = courseProp;

	// [getters, setters]
	// count = 0;
	const [count, setCount] = useState(0);
	const [seats, setSeats] = useState(10);
	const [isOpen, setIsOpen] = useState(true);

	//console.log(useState(0)); // [0, F] -> [count=0, setCount=f]
	// console.log(count);
	// setCount(100);
	// console.log(count);

	function enroll() {
		setCount(count + 1);
		console.log('Enrollees: ' + count);
		setSeats(seats - 1);

		// if(seats === 1){ if theres no use effect
		// setIsOpen(false);
	}

	useEffect(() => {
		if(seats === 0){
			setIsOpen(false);
		}
	}, [seats]);

		return(
			<Card>
				<Card.Body>
					<Card.Title>{name}</Card.Title>
					<Card.Text>
						<span className="subtitle">Description: </span>
						{description} <br />
						<span className="subtitle">Price: </span>
						PhP {price} <br />
						<span className="subtitle">Start Date: </span>
						{start_date} <br />
						<span className="subtitle">End Date: </span>
						{end_date} <br />

						Enrollees: {count} <br />
						Seats: {seats}

					</Card.Text>
					{
						isOpen ? <Button className="bg-primary" onClick = {enroll}>Enroll!</Button> 
						:
						<Button className="bg-danger" disabled>Not available</Button> 
					}
				</Card.Body>
			</Card>
		);
}

// checks if the Course component is getting the correct prop structure/data type
CourseCard.propTypes = {
		// shape() used to check if a prop object conforms or is the same to a specific "shape"/data structure/type
		course: PropTypes.shape({
		name: PropTypes.string.isRequired,
		description: PropTypes.string.isRequired,
		price: PropTypes.number.isRequired
	})
}


